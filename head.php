<!DOCTYPE html>

<html lang="en">
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 (experimental) for Mac OS X https://github.com/w3c/tidy-html5/tree/c63cc39">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
  
  <title>SMGR: Slime Mold Graph Repository</title><!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/sticky_footer.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <link href="css/expandy.css" rel="stylesheet">
  <script src="js/jquery-3.1.0.min.js"></script>
</head>

<body>
  <nav id="header" class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=
        "#navbar" aria-expanded="false" aria-controls="navbar"><span class="sr-only">Toggle
        navigation</span></button> <span class="navbar-brand"><a href="index.php">SMGR</a></span>
      </div>

      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="active">
            <a href="mission.php">Mission</a>
          </li>

          <li class="active">
            <a href="contributors.php">Contributors</a>
          </li>

          <li class="active">
            <a href="data.php">Browse/Download Data</a>
          </li>

          <li class="active">
            <a href="faq.php">FAQ</a>
          </li>

          <li class="active">
            <a href="credits.php">Acknowledgments</a>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li class="active">
            <a href="contact.php">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
