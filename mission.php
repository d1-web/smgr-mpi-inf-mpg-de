<?php include('head.php'); ?>

  <div class="container">
    <!-- Main component for a primary marketing message or call to action -->

  <h1>Our Mission</h1>
  <p>
      Slime molds such as <a href="https://en.wikipedia.org/wiki/Physarum_polycephalum"><em>P. polycephalum</em></a> that form impressive <b>networks</b> have proven to be an exciting substrate for interdisciplinary research. The study of the <b>structure and function</b> of theses organism's intricate vein-networks has captured the interest of biologists, physicists and computer scientists alike. 
  </p>
  <p>
      A popular two-step approach consists of <b>capturing images</b> of slime mold networks and converting them to <b>mathematical graphs</b> enabling a detailed
      investigation of their properties. The first part relies on cultivating specimen in the lab whilst taking images documenting the development of their networks. Obtaining such series of images is a process, which needs to be repeated many times in order to obtain a reliable body of observations. Naturally, such data acquisition comes at a <b>considerable cost in terms of time and resources</b>.
  </p>
  <p>
      In the past <b>graph-based approaches</b> have been quite successful and various interesting results are readily available online. However, the data used to establish these results, i.e. graphs and their underlying images, is not nearly as available and remains overlooked or invisible in many cases. This is most unfortunate because due to their <b>ease of handling</b> and their <b>abstraction power</b>, graphs naturally lend themselves to <b>reuse</b>, potentially gaining impact beyond their initial publications or even beyond their domain of origin.
  </p>
  <p>
      To encourage and facilitate the <b>reuse of experimental data</b> we start this repository, the <b>Slime Mold Graph Repository</b>, providing a central location for research-grade data revolving around network-forming slime molds. We believe that cultivating a small but dedicated data repository is one way to <b>enable the research community</b> to make the most of their hard-earned data. After all … sharing <em>is</em> caring.
  </p>

  </div>

  <div class="spacer-huge"></div>
  <?php include('footer.php'); ?>
</body>
</html>
