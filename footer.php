<footer class="footer" style="left:0px;">
  <div class="container" style="margin-top:9px">
    <a href="//www.mpi-inf.mpg.de/departments/algorithms-complexity/">
      <img src="./images/mpilogo-inf-wide.png" alt="mpi logo"></a>
    <div style="float:right">
      <p style="font-size:large;">
        <a href="//imprint.mpi-klsb.mpg.de/inf/smgr.mpi-inf.mpg.de">Imprint / Impressum</a>
        &nbsp;&#124;&nbsp;
        <a href="//data-protection.mpi-klsb.mpg.de/inf/smgr.mpi-inf.mpg.de">Data Protection / Datenschutzhinweis</a>
      </p>
    </div>
  </div>
</footer>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
