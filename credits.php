<?php include('head.php'); ?>

  <div class="container">
    <!-- Main component for a primary marketing message or call to action -->

    <h1>Acknowledgments</h1>

    <p>At this point the developers would like to mention and credit some of the many people who
    have been helping us in different ways during the course of this project. Without their help
    and encouragement the Slime Mold Graph Repository would not have happened.</p>

    <h4>Prof. Tetsuo Ueda</h4>

    <p>We are very grateful towards Prof. Ueda for introducing us to the interesting world of slime
    molds, for generously sharing his experiences and insights and for providing us with our very
    first samples of P. polycephalum. We sincerely hope that our software will be of use to the
    people in the field which he helped to pioneer.</p>

    <h4>Prof. Martin Grube and Dr. Christian Westendorf</h4>

    <p>We'd like to thank <a href=
    "http://uni-graz.at/~grubem/">Prof. M.
    Grube</a> for cordially inviting us to visit him and his group in Graz to talk about all
    things Physarum. </p>

    <p>We thank <a href ="https://online.uni-graz.at/kfu_online/wbForschungsportal.cbShowPortal?pPersonNr=95579&pMode=E">Dr. C. Westendorf</a> for reviewing our work and contributing with a wealth of helpful suggestions.</p>

    <h4>Prof. Dr. Andreas Manz and Mag. Tim Mehlhorn</h4>

    <p>We are happy to thank <a href=
    "http://www.frias.uni-freiburg.de/de/das-institut/archiv-frias/school-of-softmatter/fellows/manz">
    Prof. A. Manz</a> at the <a href="http://www.kist-europe.de/index.php/en/">KIST Europe</a> for
    providing us with the lab space and the equipment necessary to conduct our own experiments with
    P. Polycephalum. Without the material that originated at the KIST this repository would not have
    been possible.</p>

    <p>We are glad to work together with Mag. Mehlhorn who contributed crucially to this project by
    culturing P. Polycephalum and taking high quality images.</p>

    <h4>The IST Team at the MPI</h4>

    <p>We are particularily grateful to P. Cernko and F. Endner-Dühr for helping us navigate through the technical challenges posed by setting up a webservice like the SMGR. Whithout them this project probably wouldn't be available for the public right now.</p>

    <h4>Our students</h4>

    <p>We thank our students P. Reichert and D. Groß for actively developing this website and configuring the server it runs on.</p>

    <h4>Prof. Dr. Kurt Mehlhorn</h4>

    <p>We are very grateful to be the students of <a href="https://people.mpi-inf.mpg.de/~mehlhorn">Prof. Kurt Mehlhorn</a> at the <a href="http://www.mpi-inf.mpg.de/home/">MPI for Informatics</a>. Without his guidance, patience,
    support as well as the freedom he gives us in realizing our projects, none of this would have
    been possible.</p>

    <h1>Further credits</h1>

    <h4>h5ai</h4>

    <p>We acknowledge <a href="https://larsjung.de/h5ai/">L. Jung</a> and his excellent <a href="https://github.com/lrsjng/h5ai">h5ai project</a> for providing the core funtionalities of this scientific repository.</p>
    <a class="github-button" href="https://github.com/lrsjng/h5ai" data-icon="octicon-eye" data-style="mega" data-count-href="/lrsjng/h5ai/watchers" data-count-api="/repos/lrsjng/h5ai#subscribers_count" data-count-aria-label="# watchers on GitHub" aria-label="Watch lrsjng/h5ai on GitHub">Watch</a>
    <a class="github-button" href="https://github.com/lrsjng/h5ai/fork" data-icon="octicon-repo-forked" data-style="mega" data-count-href="/lrsjng/h5ai/network" data-count-api="/repos/lrsjng/h5ai#forks_count" data-count-aria-label="# forks on GitHub" aria-label="Fork lrsjng/h5ai on GitHub">Fork</a>
    <a class="github-button" href="https://github.com/lrsjng/h5ai/issues" data-icon="octicon-issue-opened" data-style="mega" data-count-api="/repos/lrsjng/h5ai#open_issues_count" data-count-aria-label="# issues on GitHub" aria-label="Issue lrsjng/h5ai on GitHub">Issue</a>
    <h4>Apache webserver</h4>
    <p>We rely on the open source <a href="https://httpd.apache.org/">apache webserver</a> project to run the smgr and this web page.</p>
   <h4>Icons</h4>
    <p>Icons made by Situ Herrera, Appzgear, Freepik, Google from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></p>
  </div><!-- /container -->
  <div class="spacer-huge"></div>
  <?php include('footer.php'); ?>
</body>
</html>
