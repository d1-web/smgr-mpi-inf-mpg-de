<?php include('head.php'); ?>

  <div class="jumbotron">
    <div class="container">
      <!-- Main component for a primary marketing message or call to action -->

      <h1>The Slime Mold Graph Repository</h1>

      <p>We present the Slime Mold Graph Repository, a data collection promoting the visibility, accessibility and reuse of experimental data concerned with network-forming slime molds. By making data available across disciplines, the SMGR enables the reproduction of original results as well as novel research.</p>

    </div>

  </div><!-- /container -->

  <div class="container">
    <!-- Example row of columns -->

    <div class="row">

      <div class="col-md-4">
        <h2>Mission <a class="btn btn-lg btn-primary" style="float:right" href="mission.php"
        role="button">Mission »</a></h2>

        <p>Learn what we are doing.</p>
      </div>

      <div class="col-md-4">
        <h2>Data sets<a class="btn btn-lg btn-primary" style="float:right" href="data.php" role=
        "button">Data »</a></h2>

        <p>Browse and download available data.</p>
      </div>

      <div class="col-md-4">
        <h2>Contributors<a class="btn btn-lg btn-primary" style="float:right" href="contributors.php"
        role="button">Contributors »</a></h2>

        <p>People and organisations sharing their data.</p>
      </div>
    </div><!-- row -->
  </div>
  <div class="spacer-huge"></div>
  <?php include('footer.php'); ?>
</body>
</html>
