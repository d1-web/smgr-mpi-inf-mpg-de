# smgr_website

## Externally editing this repository

Please don't make changes directly to this repository.
And if you must, please leave it in a "git clean" state.

This repo is configured so that you can directly push to it like this:

    git pull myxa:/local/smgr.mpi-inf.mpg.de/ myxa-website
    cd myxa-website
    # do some work and commit it
    git push
    # Ta-daa!

This works due to [**receive.denyCurrentBranch updateInstead**](https://github.com/git/git/blob/v2.3.0/Documentation/config.txt#L2155),
which I found on [StackOverflow](https://stackoverflow.com/a/28381311/3070326).

## Current MIRROR of this repository

Because I distrust everyone and everything, there is yet another copy of this repository at:

    https://gitlab.mpi-klsb.mpg.de/d1-web/smgr-mpi-inf-mpg-de

Good luck.
