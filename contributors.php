<?php include('head.php'); ?>

  <div class="container">
    <!-- Main component for a primary marketing message or call to action -->

    <h1>Contributors</h1>

    <p>Here we list all the individuals and groups that have kindly contributed to this repository.</p>

    <div class="container">
      <table class="table">
        <thead>
          <tr>
            <th>UID</th>
            <th>Size</th>
            <th>Name</th>

            <th>Contributors</th>
            <th>Associated publications</th>

            <!-- <th>URL</th> -->
          <!--   <th>Date</th> -->
            <!-- <th>Views</th> -->


          </tr>
        </thead>

        <tbody>
          <tr>

          <td>001</td>
          <td>~ 500 GB</td>
            <td>
            <a href="kist_data.php">KIST Europe data set</a>
            </td>

            <td><a href="http://www.kist-europe.de/index.php/de/organization/nano-engineering-group/105-mehlhorn-tim">T. Mehlhorn</a>, <a href="http://mpi-inf.mpg.de/~mtd">M. Dirnberger</a></td>
            <td><a href="/documents/kist_data/JoP_D_smgr.pdf">submitted</a>, in preparation</td>
           

           <!--  <td>
              <a href="kist_data.html">Details</a>
            </td> -->

            <!-- <td>20.11.2015</td> -->
            <!-- <td>0</td> -->
          </tr>    
        </tbody>
      </table>
    </div>
  </div>

  <div class="spacer-huge"></div>
  <?php include('footer.php'); ?>
</body>
</html>
