<?php include('head.php'); ?>

  <div class="container">
    <!-- Main component for a primary marketing message or call to action -->

    <h1>Data sets</h1>

    <p>NEFI is available free of charge under <a href="licence.html">a permissive BSD licence</a>.
    You are free to use and modify the software as you see fit. If you publish research that was
    done with the help of NEFI, we would appreciate it if you cited us as described in the <a href=
    "guide.html#citing">How to cite</a> section of the guide.</p>

    <p><!-- We provide installers for Windows, but NEFI also runs on Linux and Mac OS.  -->Should
    something not work as expected, please <a href="contact.html">contact us</a>.<br>
    <strong>Have a look at our <a href="guide.html#gettingstarted">Getting Started</a> guide for a
    quick overview how to use NEFI.</strong></p>

    <h2>Source Code</h2>

    <p>For now, only the source code is available and you need to install NEFI by hand. But fear
    not, the process in quite straightforward.</p>

    <div class="container">
      <table class="table">
        <thead>
          <tr>
            <th>UID</th>
            <th>Name</th>

            <th>Description</th>

            <th>Tags</th>
            <th>URL</th>
            <th>Date</th>
            <th>Views</th>


          </tr>
        </thead>

        <tbody>
          <tr>

          <td>001</td>

            <td>
            Node Tracked Time Series
            </td>

            <td>Time series of P. polycephalum networks. Node identities have been established across the time series.</td>

            <td>Physarum, awesome, nefi, bla bla</td>

            <td>
              <a href="">Details</a>
            </td>

            <td>20.11.2015</td>
            <td>0</td>

          </tr>    
          <tr>
          </tr>


        </tbody>
      </table>
    </div>
  </div>
  <?php include('footer.php'); ?>
</body>
</html>
